<?php
/**
 * https://gitlab.com/backvox/migrator
 * @package Voxonics\Migrator\Database\Migrations
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Migrator\Database\Migrations;

use Illuminate\Database\Migrations\Migrator as LaravelMigrator;
use Illuminate\Support\Collection;
use Illuminate\Database\Migrations\MigrationRepositoryInterface;
use Illuminate\Database\ConnectionResolverInterface;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Events\Dispatcher;

/**
 * Class Migrator
 */
class Migrator extends LaravelMigrator
{
    /**
     * @var bool
     */
    protected $isMigrate = true;

    /**
     * @var array
     */
    protected $usedMigrations = [];

    /**
     * Get all of the custom migration paths if there is not used in migration lifetime.
     *
     * @return array
     */
    public function paths()
    {
        $notUsedMigrationPath = [];
        \collect($this->paths)->each(function ($path) use (&$notUsedMigrationPath) {
            $files = \array_flip($this->getMigrationFiles($path));

            if (!empty(\array_diff($files, $this->getRun()))) {
                $notUsedMigrationPath[] = $path;
            }

        });

        return $notUsedMigrationPath;
    }

    /**
     * {@inheritDoc}
     *
     * @param  array  $migrations
     * @param  array  $options
     */
    public function runPending(array $migrations, array $options = []): void
    {
        if (empty($migrations)) {
            $this->isMigrate = false;
        }

        parent::runPending($migrations, $options);

        $this->setRun($migrations);
    }

    /**
     * Get the migration files that have not yet run.
     *
     * @param  array  $files
     * @param  array  $ran
     *
     * @return array
     */
    protected function pendingMigrations($files, $ran): array
    {
        return parent::pendingMigrations($files, $this->getRun($ran));
    }

    /**
     * Get already used migrations
     *
     * @param array $run
     *
     * @return array
     */
    protected function getRun(array $run = []): array
    {
        return \array_merge($this->usedMigrations, $run);
    }

    /**
     * Set used migrations
     *
     * @param array $run
     *
     * @return static
     */
    protected function setRun(array $run): self
    {
        \collect($run)->each(function ($migration) {
            $this->usedMigrations[] = $this->getMigrationName($migration);
        });

        return $this;
    }

    /**
     * @return bool
     */
    public function isMigrate(): bool
    {
        return $this->isMigrate;
    }
}
