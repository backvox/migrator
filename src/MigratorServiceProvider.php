<?php
/**
 * @link https://gitlab.com/backvox/migrator
 * @package Voxonics\Migrator
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Migrator;

use Voxonics\Migrator\Database\Migrations\Migrator;
use Illuminate\Database\MigrationServiceProvider;
use Voxonics\Migrator\Console\Migrations\MigrateCommand;

/**
 * Class MigratorServiceProvider
 */
class MigratorServiceProvider extends MigrationServiceProvider
{
    /**
     * {@inheritDoc}
     */
    protected function registerMigrator(): void
    {
        // The migrator is responsible for actually running and rollback the migration
        // files in the application. We'll pass in our database connection resolver
        // so the migrator can resolve any of these connections when it needs to.
        $this->app->singleton('migrator', function ($app) {
            $repository = $app['migration.repository'];
            
            return \App::makeWith(Migrator::class, [
                'repository' => $repository,
                'resolver' => $app['db'],
                'files' => $app['files'],
                'dispatcher' => $app['events'],
            ]);
        });
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerMigrateCommand()
    {
        $this->app->singleton('command.migrate', function ($app) {
            return \App::makeWith(MigrateCommand::class, ['migrator' => $app['migrator']]);
        });
    }
}
