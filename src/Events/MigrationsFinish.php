<?php
/**
 * https://gitlab.com/backvox/migrator
 * @package Voxonics\Migrator\Events
 * @author Denys <AikrofStark@gmail.com>
 */

declare(strict_types=1);

namespace Voxonics\Migrator\Events;

use Illuminate\Contracts\Database\Events\MigrationEvent as MigrationEventContract;

/**
 * Class MigrationsFinish
 */
class MigrationsFinish implements MigrationEventContract
{
    //
}
